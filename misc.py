from configparser import ConfigParser
from pathlib import Path
import sys
from os import system

VERSION = "1.4"
URL = r"https://gitlab.com/Shoun2137/ztexipy"
PROGRAM_TITLE = f"[zTEXiPy {VERSION}]"

config_template = """
[MISC]
; Enables debug mode, this could harm performance.
debug=0

; Default color scheme (json file)
theme=teal.json

; Default font and size
font=NotoSans-Regular.ttf
fontsize=22.5

; Specifies how many of CPU cores zTEXiPy should use (in procentage)
; Default: 60 - On system with 12 cores, it'll use 7 cores.
cpu_core_usage=60

; This is a handler that registers image formats, you can add your own here if Pillow supports it.
common_image_supported_write=TGA DDS PNG WEBP

[LIBSQUISH]
; look at squish(IntEnum) class for flag description
flags=256

; Specifies name override of the libsquish library without extension
; Default: libsquish-bind.x64
; From 1.3.6 this option is here just for making my life easier when testing.
; There's no need to change libraries because it was all optimised to hell.
libraryname=libsquish-bind.x64

[LEENOOKS]
; Force XDG portal support for GTK framework, because fuck GTK4 and by extension fuck zenity 3.90+.
GTK_USE_PORTAL=1
GDK_DEBUG=portals
"""


# We have to pick proper path, because with single exe mode pyinstaller unpacks entire program on fly in the temp folder
if getattr(sys, "frozen", False) and hasattr(sys, "_MEIPASS"):
    # Set the file at the current working directory
    CURRENT_EXECUTABLE = Path(sys.executable)
    CURRENT_PATH = CURRENT_EXECUTABLE.parent
    # sys._MEIPASS doesn't work here as that points to `_internal` folder. We don't want that.
else:
    # Set the file beside the .py script
    CURRENT_PATH = Path(".").resolve()
    CURRENT_EXECUTABLE = CURRENT_PATH / "init.py"


def get_config():
    handle = CURRENT_PATH / "config.ini"
    config = ConfigParser(allow_no_value=True)
    if handle.exists() and handle.stat().st_size != 0:
        config.read(handle)
    else:
        with open(handle, "w") as configfile:
            configfile.write(config_template)
        config.read_string(config_template)
    return config


CONFIG_HANDLE = get_config()
DEBUG = bool(int(CONFIG_HANDLE["MISC"]["debug"]))
FONT_SIZE = float(CONFIG_HANDLE["MISC"]["fontsize"])
FONT = CONFIG_HANDLE["MISC"]["font"]
THEME = CONFIG_HANDLE["MISC"]["theme"]

if sys.platform == "linux":
    from os import environ

    # https://github.com/hoffstadt/DearPyGui/issues/554#issuecomment-1852406840
    environ["__GLVND_DISALLOW_PATCHING"] = "1"

    # Force portal envs because i don't fucking care about GTK and GNOME bullshit
    # If filepicker doesn't work, that's purely because of zenity's GTK4
    environ["GTK_USE_PORTAL"] = CONFIG_HANDLE["LEENOOKS"]["GTK_USE_PORTAL"]
    environ["GDK_DEBUG"] = CONFIG_HANDLE["LEENOOKS"]["GDK_DEBUG"]

    exit_cmd = r"read -rsp $'Press any key to continue...\n' -n 1 key"
else:
    exit_cmd = "pause"

def graceful_exit(message=""):
    print(message)
    system(exit_cmd)
    sys.exit()