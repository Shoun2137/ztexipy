# zTEXiPy - ZenGin texture converter

from PIL import Image
from PIL._binary import i32le, o32le
from squish import compress_image
from zTextureFormat import FORMAT_PRINT_LOOKUP, zTex
from time import perf_counter
from misc import DEBUG

SIZE_CORRECTION_MODE = ("EXTEND", "RESIZE")

VERBOSE = True

SUPPORTED_COLORSPACE = (
    zTex.BGRA_8888,
    zTex.ARGB_8888,
    zTex.ABGR_8888,
    zTex.RGBA_8888,
    zTex.BGR_888,
    zTex.RGB_565,
    zTex.DXT1,
    zTex.DXT3,
    zTex.DXT5,
)

# It's all static cos of portability.
class zTexture:
    @staticmethod
    def load_header(filename):
        with open(filename, "rb+") as fp:
            # Gathering Info
            header = fp.read(36)
            magic_and_version = header[:8]
            if magic_and_version != b"ZTEX\x00\x00\x00\x00":  # Always ZTEX and 0
                return None

            format = i32le(header[8:12])
            width = i32le(header[12:16])
            height = i32le(header[16:20])
            mipmap_count = i32le(header[20:24])
            refwidth = i32le(header[24:28])
            refheight = i32le(header[28:32])
            average_color = i32le(header[32:36])
        return (format, width, height, mipmap_count, refwidth, refheight, average_color)

    @staticmethod
    def save(
        image: Image,
        filename: str,
        generate_mipmaps=True,
        format_param: int = None,
        size_correction_mode="EXTEND",
    ):
        def _correct_pow_size(width, height):
            x = 2 ** ((width - 1).bit_length())
            y = 2 ** ((height - 1).bit_length())
            return x, y

        def _get_mipmap_count(width: int, height: int):
            count = 1
            while width > 4 and height > 4:
                count += 1
                width >>= 1
                height >>= 1
            return count

        def _process_mipmap(image, format):
            if format in (zTex.DXT1, zTex.DXT3, zTex.DXT5):
                w, h = image.size
                if w < 4 or w % 4 != 0 or h < 4 or h % 4 != 0:
                    raise Exception(
                        f"Invalid mipmap generation with size {w}x{h}. Image must have dimensions that are multiples of 4."
                    )
                # WARNING! libsquish works with input of 32bit RGBA8888 ONLY!!!
                image = image.convert("RGBA")
                return compress_image(image, format)
            elif format == zTex.RGBA_8888:  # good
                if image.mode == "RGB":
                    image = image.convert("RGBA")
                return Image.frombytes(
                    "RGBA", image.size, image.tobytes(), "raw", "RGBA"
                ).tobytes()
            elif format == zTex.BGRA_8888:  # good
                if image.mode == "RGB":
                    image = image.convert("RGBA")
                return Image.frombytes(
                    "RGBA", image.size, image.tobytes(), "raw", "BGRA"
                ).tobytes()
            elif format == zTex.ARGB_8888:  # good
                if image.mode == "RGB":
                    image = image.putalpha(1)
                r, g, b, a = image.split()
                return Image.merge("RGBA", (a, r, g, b)).tobytes()
            elif format == zTex.ABGR_8888:
                if image.mode == "RGB":
                    image = image.putalpha(1)
                r, g, b, a = image.split()
                return Image.merge("RGBA", (a, b, g, r)).tobytes()
            elif format == zTex.RGB_565:
                return image.convert("BGR;16").tobytes()
                # return image.convert("RGB;565").tobytes() # https://github.com/python-pillow/Pillow/pull/8158
            elif format == zTex.BGR_888:
                if image.mode == "RGBA":
                    r, g, b, _ = image.split()
                else:
                    r, g, b = image.split()
                return Image.merge("RGB", (b, g, r)).tobytes()

        if DEBUG:
            start = perf_counter()

        if image.mode not in ("RGBA", "RGB"):
            raise Exception(
                f"Invalid image mode: Unsupported color space, convert to either RGB or RGBA"
            )

        if image.mode == "RGB":
            format = zTex.DXT1
        if image.mode == "RGBA":
            format = zTex.DXT3

        if format_param is not None:
            if zTex(format_param) in SUPPORTED_COLORSPACE:
                format = format_param
            else:
                print(
                    f"Invalid format - {format_param} - chosen for writing the texture."
                )
                return False

        width, height = _correct_pow_size(image.size[0], image.size[1])
        if size_correction_mode == SIZE_CORRECTION_MODE[1]:
            image = image.resize((width, height))
        else:
            temp_image = Image.new(mode=image.mode, size=(width, height))
            temp_image.paste(image, (0, 0))
            image = temp_image

        # ↓↓↓ Sketchy support of NOMIP in paths or filenames just like in ZenGin!
        if "NOMIP" in str(filename).upper():
            generate_mipmaps = False

        textures = bytes()
        if generate_mipmaps:
            mipmap_count = _get_mipmap_count(width, height)
            for mip_id in range(mipmap_count - 1, 0, -1):
                mip_width = max(1, width >> mip_id)
                mip_height = max(1, height >> mip_id)
                mipmap = image.resize((mip_width, mip_height))
                textures += _process_mipmap(mipmap, format)
        else:
            mipmap_count = 1

        textures += _process_mipmap(image, format)  # zero-level mipmap = full texture

        assembled_file = (  # Bytestring
            b"ZTEX\x00\x00\x00\x00"  # magic and version
            + o32le(format)
            + o32le(width)
            + o32le(height)
            + o32le(mipmap_count)
            + o32le(width)  # refwidth
            + o32le(height)  # refheight
            + o32le(
                2976579765
            )  # average color can be left empty, feel free to add support :shrug:
            + textures
        )

        if VERBOSE:
            print(
                "Saving:",
                filename,
                f"(format: {FORMAT_PRINT_LOOKUP[format]})",
                f"({width}x{height})",
                f"(mipmaps: {mipmap_count})",
                f"(size_correction_mode: {size_correction_mode})",
            )

        # Gather all data in place, then dump it to file = less I/O overhead
        with open(filename, "wb+") as fp:
            fp.write(assembled_file)

        if DEBUG:
            duration = perf_counter() - start
            print(f"Took: {duration:.2f}s")

    @staticmethod
    def load(filename, generate_mipmaps=True):
        if DEBUG:
            start = perf_counter()

        with open(filename, "rb+") as fp: 
            # Gathering Info
            header = fp.read(36)
            magic_and_version = header[:8]
            if magic_and_version != b"ZTEX\x00\x00\x00\x00":  # Always ZTEX and 0
                return None

            format = i32le(header[8:12])
            width = i32le(header[12:16])
            height = i32le(header[16:20])
            mipmap_count = i32le(header[20:24])
            # refwidth = i32le(header[24:28])
            # refheight = i32le(header[28:32])
            average_color = i32le(header[32:36])

            if format == zTex.PAL_8:
                raise Exception("Palette8 not implemented.")

            if VERBOSE:
                print(
                    "Loading:",
                    filename,
                    f"(format: {FORMAT_PRINT_LOOKUP[format]})",
                    f"({width}x{height})",
                    f"(AvgColor:{hex(average_color)})",
                    f"(mipmaps: {mipmap_count})",
                )

            # Extracting mipmaps
            textures = list()
            combined_mipmap_size = fp.tell()
            for level in range(mipmap_count - 1, 0, -1):
                mipmap_size, dimension = zTexture.compute_mipmap_size(
                    format, width, height, level
                )
                combined_mipmap_size += mipmap_size
                if generate_mipmaps:
                    textures.append((fp.read(mipmap_size), dimension))

            if fp.tell() != combined_mipmap_size:
                fp.seek(combined_mipmap_size)

            #    ↓↓↓ zero-level mipmap = full texture ↓↓↓
            textures.append((fp.read(), (width, height)))

            final_image = zTexture.as_image(format, textures)
            if DEBUG:
                duration = perf_counter() - start
                print(f"Took: {duration:.2f}s")
            return final_image

    @staticmethod
    def compute_mipmap_size(format, width, height, level):
        x = int(max(1, width))
        y = int(max(1, height))

        for i in range(0, level):
            if x > 1:
                x >>= 1
            if y > 1:
                y >>= 1

        if format in (zTex.ARGB_8888, zTex.ABGR_8888, zTex.RGBA_8888, zTex.BGRA_8888):
            return x * y * 4, (x, y)
        elif format in (zTex.RGB_888, zTex.BGR_888):
            return x * y * 3, (x, y)
        elif format in (zTex.ARGB_4444, zTex.ARGB_1555, zTex.RGB_565):
            return x * y * 2, (x, y)
        elif format == zTex.PAL_8:
            return x * y, (x, y)
        elif format == zTex.DXT1:
            return int(max(1, x / 4) * max(1, y / 4) * 8), (x, y)
        elif format in (zTex.DXT3, zTex.DXT5):
            return int(max(1, x / 4) * max(1, y / 4) * 16), (x, y)

        return 0, (x, y)

    @staticmethod
    def as_image(format, textures):
        converted_data = list()
        for texture, dimension in textures:
            try:
                if format == zTex.ABGR_8888:
                    converted_data.append(
                        Image.frombytes("RGBA", dimension, texture, "raw", "ABGR")
                    )
                elif format == zTex.ARGB_8888:
                    converted_data.append(
                        Image.frombytes("RGBA", dimension, texture, "raw", "ARGB")
                    )
                elif format == zTex.RGBA_8888:
                    converted_data.append(
                        Image.frombytes("RGBA", dimension, texture, "raw", "RGBA")
                    )
                elif format == zTex.BGRA_8888:
                    converted_data.append(
                        Image.frombytes("RGBA", dimension, texture, "raw", "BGRA")
                    )
                elif format == zTex.ARGB_1555:
                    converted_data.append(
                        Image.frombytes("RGBA", dimension, texture, "raw", "RGBA;15")
                        #Image.frombytes("RGBA", dimension, texture, "raw", "ABGR;1555") # https://github.com/python-pillow/Pillow/pull/8158
                    )
                elif format == zTex.ARGB_4444:
                    # Pillow doesn't support ARGB4444, let's just swap channels, should suffice
                    image = Image.frombytes(
                        "RGBA", dimension, texture, "raw", "RGBA;4B"
                        # "RGBA", dimension, texture, "raw", "ABGR;4" # https://github.com/python-pillow/Pillow/pull/8158
                    )  # tested!
                    r, g, b, a = image.split()
                    image = Image.merge("RGBA", (b, g, r, a))
                    converted_data.append(image)
                elif format == zTex.RGB_888:
                    converted_data.append(
                        Image.frombytes("RGB", dimension, texture, "raw", "BGR")
                    )
                elif format == zTex.BGR_888:
                    converted_data.append(
                        Image.frombytes("RGB", dimension, texture, "raw", "RGB")
                    )
                elif format == zTex.RGB_565:
                    converted_data.append(
                        Image.frombytes("RGB", dimension, texture, "raw", "BGR;16")
                        # Image.frombytes("RGB", dimension, texture, "raw", "RGB;565") # https://github.com/python-pillow/Pillow/pull/8158
                    )  # tested!
                elif format == zTex.DXT1:
                    image = Image.frombytes(
                        "RGBA", dimension, texture, "bcn", (1, "BCN")
                    )  # DXT1A (BC1)
                    converted_data.append(image.convert("RGB"))
                elif format == zTex.DXT3:
                    converted_data.append(
                        Image.frombytes("RGBA", dimension, texture, "bcn", (2, "BCN"))
                    )  # DXT3 (BC2)
                elif format == zTex.DXT5:
                    converted_data.append(
                        Image.frombytes("RGBA", dimension, texture, "bcn", (3, "BCN"))
                    )  # DXT5 (BC3)
                elif format in (zTex.DXT4, zTex.DXT2, zTex.PAL_8):
                    raise Exception("Reading Palette8, DXT2, and DXT4 not supported.")
            except ValueError as e:
                print(f"Corrupted mipmap found! {e}")
                converted_data.append(None)
        return converted_data
