# -*- mode: python ; coding: utf-8 -*-
# For LINUX build.

excluded_libraries = [
    'tkinter', # bloats the package... not needed here
]

a = Analysis(
    ['init.py'],
    pathex=[],
    binaries=[],
    datas=[],
    hiddenimports=[],
    hookspath=[],
    hooksconfig={},
    runtime_hooks=[],
    excludes=excluded_libraries,
    noarchive=False,
)
pyz = PYZ(a.pure)

exe = EXE(
    pyz,
    a.scripts,
    [],
    exclude_binaries=True,
    name='zTEXiPy',
    debug=False,
    bootloader_ignore_signals=False,
    strip=True, # This is a must for LINUX
    upx=True,
    console=True,
    disable_windowed_traceback=False,
    argv_emulation=False,
    target_arch=None,
    codesign_identity=None,
    entitlements_file=None,
    icon=['icon.ico'],
)
coll = COLLECT(
    exe,
    a.binaries,
    a.datas,
    strip=True,
    upx=True,
    upx_exclude=[],
    name='linux',
)
