# Simple toolchain file for compiling under windows
# Usage: cmake .. -DCMAKE_TOOLCHAIN_FILE=windows-x64.cmake
# Take a look at compilation flags in linux and mingw .cmake files,
# because msvc side of things wasn't that much optimised.

# specify the target system properties
set(CMAKE_SYSTEM_NAME Windows)

if (CMAKE_GENERATOR MATCHES "Visual Studio*")
	set(CMAKE_GENERATOR_PLATFORM x64 CACHE INTERNAL "")
endif()

# specify shared library suffix
set(OUT_FILE_SUFFIX "x64")
