# Sample toolchain file for building for Windows from an Linux system.
# Usage:
# mkdir build_windows; cd build_windows
# cmake .. -DCMAKE_TOOLCHAIN_FILE=../mingw-w64-x86_64.cmake && make

set(CMAKE_SYSTEM_NAME Windows)
set(TOOLCHAIN_PREFIX x86_64-w64-mingw32)

# specify shared library suffix
set(OUT_FILE_SUFFIX "mingw64")

# cross compilers to use for C, C++ and Fortran
set(CMAKE_C_COMPILER ${TOOLCHAIN_PREFIX}-gcc)
set(CMAKE_CXX_COMPILER ${TOOLCHAIN_PREFIX}-g++)
set(CMAKE_Fortran_COMPILER ${TOOLCHAIN_PREFIX}-gfortran)
set(CMAKE_RC_COMPILER ${TOOLCHAIN_PREFIX}-windres)

# target environment on the build host system
set(CMAKE_FIND_ROOT_PATH /usr/${TOOLCHAIN_PREFIX})

# modify default behavior of FIND_XXX() commands
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)

# specify the compiler flags
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wall -Wextra -O2 -s -flto -fno-unroll-loops -fno-exceptions -fno-asynchronous-unwind-tables -ffunction-sections -fdata-sections -fno-rtti -fno-sized-deallocation -fno-math-errno -static-libgcc -static-libstdc++ -fPIC -msse2")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -O2 -s -flto -fno-unroll-loops -fno-exceptions -fno-asynchronous-unwind-tables -ffunction-sections -fdata-sections -fno-rtti -fno-sized-deallocation -fno-math-errno -static-libgcc -static-libstdc++ -fPIC -msse2")
