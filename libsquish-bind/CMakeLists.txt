cmake_minimum_required(VERSION 3.17)

project(libsquish-bind)
set (CMAKE_CXX_STANDARD 14)
set(CMAKE_MSVC_RUNTIME_LIBRARY "MultiThreaded$<$<CONFIG:Debug>:Debug>")

file(GLOB SRC
	"src/main.cpp"
	"src/main.h"
)

add_library(libsquish-bind STATIC SHARED ${SRC} "src/main.h")

target_precompile_headers(libsquish-bind
	PRIVATE
		"src/main.h"
)

target_include_directories(libsquish-bind
	PRIVATE
		"src/"
		"dependencies/libsquish"
		"${CMAKE_CURRENT_BINARY_DIR}/dependencies/libsquish"

)

add_subdirectory(dependencies)
target_link_libraries(libsquish-bind
        PUBLIC squish)
if(DEFINED OUT_FILE_SUFFIX)
	set_target_properties(libsquish-bind
			PROPERTIES 
				PREFIX ""
				SUFFIX ".${OUT_FILE_SUFFIX}${CMAKE_SHARED_LIBRARY_SUFFIX}"
	)
endif()
