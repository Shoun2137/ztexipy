# Simple toolchain file for compiling under linux
# Usage:
# mkdir build_linux; cd build_linux
# cmake .. -DCMAKE_TOOLCHAIN_FILE=../linux-x64.cmake && make

set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_PROCESSOR x86_64)

# specify shared library suffix
set(OUT_FILE_SUFFIX "x64")

# specify the compilers
set(CMAKE_C_COMPILER gcc)
set(CMAKE_CXX_COMPILER g++)

# specify the compiler flags
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wall -Wextra -O2 -s -flto -fno-unroll-loops -fno-exceptions -fno-asynchronous-unwind-tables -ffunction-sections -fdata-sections -fno-rtti -fno-sized-deallocation -fno-math-errno -static-libgcc -static-libstdc++ -fPIC -msse2")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -O2 -s -flto -fno-unroll-loops -fno-exceptions -fno-asynchronous-unwind-tables -ffunction-sections -fdata-sections -fno-rtti -fno-sized-deallocation -fno-math-errno -static-libgcc -static-libstdc++ -fPIC -msse2")

# specify the paths for find_(name), functions (target environment). 
set(CMAKE_FIND_ROOT_PATH /usr/lib)

# search only for programs in the build host directories (find_program)
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)

# use only CMAKE_FIND_ROOT_PATH for searching (find_library)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)

# use only CMAKE_FIND_ROOT_PATH for searching (find_file, find_path)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)

# use only CMAKE_FIND_ROOT_PATH for searching (find_package)
set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)
