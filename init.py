from multiprocessing import freeze_support

freeze_support()  # pyinstaller

from argparse import ArgumentParser, SUPPRESS, RawTextHelpFormatter
from time import perf_counter
from pathlib import Path
from misc import VERSION, URL
from zTexture import *  # pyinstaller...
from GUI import start_viewer
from CLI import Converter

ENUMS = "\n            ".join(
    FORMAT_PRINT_LOOKUP[e.value] for e in SUPPORTED_COLORSPACE
)  # this looks goofy af but works

USAGE = f"""

{URL}

Usage:
Convert from ZenGin .TEX
    zTEXiPy -ft <path> [output_path] -f <format> -o [overwrite]
        <path>: Path pointing to file or folder.
        [output path]: Optional output path.
        <format>: Target image conversion format, defaults to TGA
            Available formats at: https://pillow.readthedocs.io/en/stable/handbook/image-file-formats.html
        [overwrite]: zTEXiPy skips existing files by default, you can overwrite them if you specify this option.
    Example:
        zTEXiPy -ft \"E:\\Gothic\\_Work\\Data\\Textures\\_compiled\" -f webp
        zTEXiPy -ft \"E:\\Gothic\\_Work\\Data\\Textures\\_compiled\" \"E:\\Gothic\\_Work\\Data\\Textures\" -f webp -o true
    Note:
        zTEXiPy skips stored mipmaps by default.

Convert to ZenGin .TEX
    zTEXiPy -tt <path> [output_path] -f <colorspace> -o [overwrite] -gm [generate_mipmaps] -scm [size_correction_mode]
        <path>: Path pointing to file or folder.
        [output_path]: Optional output path.
        <colorspace>: ID of write supported colorspace:
            {ENUMS}
        [overwrite]: zTEXiPy skips existing files by default, you can overwrite them if you specify this option.
        [generate_mipmaps]: Override for mipmap generation
        [size_correction_mode]: Override for size correction mode.
            Essentially, when your texture's dimensions aren't up to power of 2, zTEXiPy will try to:
            - EXTEND - Texture's borders will be extended gracefully by filling them with alpha channel or black void. The default.
            - RESIZE - Texture will be physically rescaled to the closest value.
            Choose EXTEND or RESIZE as option.
    Example:
        zTEXiPy -tt \"E:\\Gothic\\_Work\\Data\\Textures\"
        zTEXiPy -tt \"E:\\Gothic\\_Work\\Data\\Textures\" \"E:\\Working\\_compiled\" -f 3 -o 1
    Note:
        zTEXiPy generates mipmaps by default, but respects NOMIP and won't generate mipmaps if it finds it in path or filename.
        zTEXiPy defaults to DXT1 (BC1) compression for RGB images and DXT3 (BC2) compression for RGBA images.
        When zTEXiPy detects compiled texture, it will try to reconvert it's internal colorspace to the one specified by format parameter.

Texture viewer:
    zTEXiPy -GUI <file>
        <file>: Location of the ZenGin .TEX
    Example:
        zTEXiPy -GUI \"E:\\gimme\\gothic\\fisch\\NW_NATURE_GREATSEA_FISH_01-C.TEX\"
    Note:
        This option opens viewer. UI Navigation: Arrows and WASD cycles through textures in the current directory.
        You can drag .TEX files from folder and drop it into the window.
        Zoom in or out with scroll, also you can zoom-select with right click.
"""


def main():
    input_path = None
    output_path = None
    results = None
    overwrite = False
    generate_mipmaps = True
    size_correction_mode = "EXTEND"
    colorspace = None
    BOOL_VAL = ("TRUE", "T", "YES", "Y", "1")
    # argparse uses *nix like help - it's bad, unreadable and messy fuckery, let's use preformated string.
    parser = ArgumentParser(
        prog="zTEXiPy",
        description=USAGE,
        usage=SUPPRESS,
        formatter_class=RawTextHelpFormatter,
    )
    parser.add_argument("path", help=SUPPRESS, action="store", nargs="?")
    parser.add_argument("-GUI", help=SUPPRESS)
    parser.add_argument("-ft", "--from-tex", nargs="+", help=SUPPRESS)
    parser.add_argument("-tt", "--to-tex", nargs="+", help=SUPPRESS)
    parser.add_argument("-f", "--format", help=SUPPRESS)
    parser.add_argument("-o", "--overwrite", help=SUPPRESS)
    parser.add_argument("-gm", "--generate_mipmaps", help=SUPPRESS)
    parser.add_argument("-scm", "--size_correction_mode", help=SUPPRESS)

    print(f"zTEXiPy {VERSION}")

    args = parser.parse_intermixed_args()

    if any(vars(args).values()) is False:
        start_viewer()
        return

    if args.path:
        start_viewer(path=args.path)
        return

    if args.GUI:
        start_viewer(path=args.GUI)
        return

    start = perf_counter()

    if args.from_tex:
        if len(args.from_tex) > 2:
            parser.print_help()
            return

        input_path = Path(args.from_tex[0])
        try:
            output_path = Path(args.from_tex[1])
        except IndexError:
            pass

        if args.format:
            print(f"Chosen format: {args.format}")

        if args.overwrite:
            overwrite = args.overwrite.upper() in ("TRUE", "T", "YES", "Y", "1")

        if input_path.is_file():
            Converter.from_tex(
                input=input_path,
                format=args.format,
                output=output_path,
                overwrite=overwrite,
            )
        else:
            results = list(
                Converter.from_tex_batch(
                    input=input_path,
                    format=args.format,
                    output=output_path,
                    overwrite=overwrite,
                )
            )
    elif args.to_tex:
        if len(args.to_tex) > 2:
            parser.print_help()
            return

        input_path = Path(args.to_tex[0])
        try:
            output_path = Path(args.to_tex[1])
        except IndexError:
            pass

        if args.format:
            try:
                colorspace = int(args.format)
                print(f"Chosen format: {FORMAT_PRINT_LOOKUP[format]}")
            except:
                pass

        if args.overwrite:
            overwrite = args.overwrite.upper() in BOOL_VAL

        if args.generate_mipmaps:
            generate_mipmaps = args.generate_mipmaps.upper() in BOOL_VAL

        if args.size_correction_mode:
            if args.size_correction_mode.upper() in SIZE_CORRECTION_MODE:
                size_correction_mode = args.size_correction_mode.upper()

        if input_path.is_file():
            Converter.to_tex(
                input_path,
                format=colorspace,
                output=output_path,
                overwrite=overwrite,
                generate_mipmaps=generate_mipmaps,
                size_correction_mode=size_correction_mode,
            )
        else:
            results = list(
                Converter.to_tex_batch(
                    input_path,
                    format=colorspace,
                    output=output_path,
                    overwrite=overwrite,
                    generate_mipmaps=generate_mipmaps,
                    size_correction_mode=size_correction_mode,
                )
            )

    duration = perf_counter() - start
    # TODO: task id, task status as a return, generating a report for batch mode
    if results:
        success = 0
        failure = 0
        for result in results:
            if result:
                success += 1
            else:
                failure += 1

        print(f"Successful processing of {success} files in {duration:.2f}s")
        if failure != 0:
            print(f"Failed: {failure}")
    else:
        print(f"Finished in processing in: {duration:.2f}s")


if __name__ == "__main__":
    main()
