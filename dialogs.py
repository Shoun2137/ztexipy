# Based on https://github.com/mathgeniuszach/xdialog/tree/main
# Extracted as a standalone file and extended a bit
import sys

def clean(data: str):
    special_characters = {
        "\\": "\\\\",
        "$": "\\$",
        "!": "\\!",
        "*": "\\*",
        "?": "\\?",
        "&": "&amp;",
        "|": "&#124;",
        "<": "&lt;",
        ">": "&gt;",
    }

    for char, replacement in special_characters.items():
        data = data.replace(char, replacement)
    return data


def filepicker(
    filepicker_type="",
    title="",
    filetypes=None,
    confirmoverwrite=True,
    initialfile=None,
    multiple=False,
):
    if sys.platform == "win32":
        from tkinter import filedialog

        if filepicker_type == "SAVE_FILE":
            return (
                filedialog.asksaveasfilename(
                    title=title,
                    filetypes=filetypes,
                    initialfile=initialfile,
                    confirmoverwrite=confirmoverwrite,
                )
                or ""
            )
        elif filepicker_type == "ASK_FILE":
            return (
                filedialog.askopenfilename(
                    title=title, filetypes=filetypes, initialfile=initialfile
                )
                or ""
            )
        elif filepicker_type == "ASK_DIRECTORY":
            return filedialog.askdirectory(title=title, mustexist=True) or ""
    if sys.platform == "linux":
        from subprocess import Popen, PIPE, STDOUT
        from os.path import isfile

        def zenity(typ, filetypes=None, **kwargs):
            # Build args based on keywords
            args = ["zenity", "--" + typ]
            for k, v in kwargs.items():
                if v is True:
                    args.append(f'--{k.replace("_", "-").strip("-")}')
                elif isinstance(v, str):
                    cv = clean(v) if k != "title" else v
                    args.append(f'--{k.replace("_", "-").strip("-")}={cv}')

            # Build filetypes specially if specified
            if filetypes:
                for name, globs in filetypes:
                    if name:
                        globlist = globs.split()
                        args.append(
                            f'--file-filter={name.replace("|", "")} ({", ".join(t for t in globlist)})|{globs}'
                        )

            with Popen(args, stdout=PIPE, stderr=STDOUT, shell=False) as process:
                return process.communicate()[0].decode("utf8").strip()

        if filepicker_type == "SAVE_FILE":
            return zenity(
                "file-selection",
                title=title,
                filetypes=filetypes,
                filename=initialfile,
                save=True,
            )
        elif filepicker_type == "ASK_DIRECTORY":
            return zenity("file-selection", title=title, directory=True)
        elif filepicker_type == "ASK_FILE":
            # Zenity is strange and will let you select folders for some reason in some cases. So we filter those out.
            if multiple:
                files = zenity(
                    "file-selection",
                    title=title,
                    filetypes=filetypes,
                    multiple=True,
                    separator="\n",
                ).splitlines()
                return list(filter(isfile, files))
            else:
                file = zenity(
                    "file-selection",
                    title=title,
                    filetypes=filetypes,
                    filename=initialfile,
                )
            if file and isfile(file):
                return file
            else:
                return ""

