from textwrap import TextWrapper
from typing import List, Tuple
from dataclasses import dataclass, field
from pathlib import Path
import dearpygui.dearpygui as dpg
import numpy as np
import sys
from misc import (
    CURRENT_PATH,
    CURRENT_EXECUTABLE,
    DEBUG,
    VERSION,
    FONT,
    FONT_SIZE,
    THEME,
    PROGRAM_TITLE,
)
from zTexture import *
from zTextureFormat import ALL_EXT, ZENGIN_EXT, IMAGE_EXT, SUPPORTED_WRITE_IMAGE_FORMATS
from dialogs import filepicker
from subprocess import Popen, PIPE, STDOUT
from json import load as json_load

FONT_PATH = CURRENT_PATH / "fonts" / FONT
THEME_PATH = CURRENT_PATH / "themes" / THEME
BATCH_MODE = ("From Tex", "To Tex")
FORMAT_MODE = ("Format:", "Colorspace:")
ZTEX_COLORSPACE = list(FORMAT_PRINT_LOOKUP[e.value] for e in SUPPORTED_COLORSPACE)
COLORSPACE_LOOKUP = dict(
    zip(ZTEX_COLORSPACE, list(e.value for e in SUPPORTED_COLORSPACE))
)
ZTEX_COLORSPACE.insert(0, "Default (DXT3 for alpha, else DXT1)")

if sys.platform == "win32":

    def drag_and_drop(cache):
        try:
            import DearPyGui_DragAndDrop as dpg_dnd
        except:
            dpg_dnd = None

        def dnd_callback(data, keys, user_data):
            path = Path(data[0])

            if not path.exists():
                return

            if path.is_file():
                dpg.set_value("main_bar", dpg.get_alias_id("viewer_tab"))

            user_data.update("dnd_callback", (data, keys), path)

        if dpg_dnd is not None:
            dpg_dnd.initialize()
            dpg_dnd.set_drop(
                function=lambda data, keys: dnd_callback(data, keys, (cache))
            )


class PopupFactory:
    def __init__(
        self,
        item_id,
        width,
        height,
        title=None,
        no_background=False,
        title_bar=True,
        show=False,
        modal=True,
        moving=False,
        resizable=False,
    ):
        self.item_id = item_id
        self.moving = moving
        self.modal = modal
        self.width = width
        self.height = height
        self.autosize = self.width <= 0
        self.label = title
        self.background = no_background
        self.title_bar = title_bar
        self.visible_event_handler = f"{self.item_id}_handler"
        self.resizable = resizable
        dpg.add_window(
            modal=self.modal,
            no_close=True,
            no_move=not self.moving,
            width=self.width,
            height=self.height,
            tag=self.item_id,
            autosize=self.autosize,
            label=self.label,
            no_title_bar=not self.title_bar,
            no_background=self.background,
            no_resize=not self.resizable,
            show=show,
            pos=[
                int((dpg.get_viewport_width() // 2 - 400 // 2)),
                int((dpg.get_viewport_height() / 2 - 400 / 2)),
            ],
        )
        if self.modal:
            dpg.add_item_handler_registry(tag=self.visible_event_handler)
            dpg.add_item_visible_handler(
                callback=self.render_window_center,
                parent=self.visible_event_handler,
                user_data=self.item_id,
            )
            dpg.bind_item_handler_registry(
                item=self.item_id, handler_registry=self.visible_event_handler
            )

    @staticmethod
    def render_window_center(sender, app_data, user_data):
        if dpg.does_item_exist(user_data):
            main_width = dpg.get_viewport_width()
            main_height = dpg.get_viewport_height()
            login_width = dpg.get_item_width(user_data)
            login_height = dpg.get_item_height(user_data)
            dpg.set_item_pos(
                user_data,
                [
                    int((main_width // 2 - login_width // 2)),
                    int((main_height / 2 - login_height / 2)),
                ],
            )


class LayoutHelper:
    def __enter__(self):
        self.table_id = dpg.add_table(
            header_row=False, policy=dpg.mvTable_SizingStretchProp
        )
        self.stage_id = dpg.add_stage()
        dpg.push_container_stack(self.stage_id)
        return self

    def add_widget(self, uuid, percentage):
        dpg.add_table_column(
            init_width_or_weight=percentage / 100.0, parent=self.table_id
        )
        dpg.set_item_width(uuid, -1)

    def __exit__(self, exc_type, exc_value, exc_traceback):
        dpg.pop_container_stack()
        with dpg.table_row(parent=self.table_id):
            dpg.unstage(self.stage_id)


def load_texture(filename: Path):
    extension = filename.suffix.lower()

    if extension in ZENGIN_EXT:
        header = zTexture.load_header(filename)
        if not header:
            print(f"[Header fault] {filename} is invalid texture...")
            return None

        texture = zTexture.load(filename)
        if not texture:
            print(f"[Parsing fault] {filename} is invalid texture...")
            return None
        return ("ZENGIN_TEXTURE", header, texture)
    elif extension in IMAGE_EXT:
        try:
            image = Image.open(filename)
            w, h = image.size
            return ("COMMON_IMAGE", [image.mode, w, h, 0, 0, 0, 0], [image])
        except:
            return None


# converts texture to numpy array for dpg to use
def convert_texture(texture):
    if texture:
        mipmaps = list()
        for mipmap in texture:
            if mipmap:
                # for some unknown reason rgba buffer has better performance with dpg (probably because dpg doesnt need to add alpha itself??? idk), also prevents crashes with RGB565
                mipmaps.append(np.array(mipmap.convert("RGBA"), dtype=np.uint8) / 255.0)
            else:
                mipmaps.append(None)
        return mipmaps
    return None


@dataclass(slots=True)
class Texture:
    header: Tuple[str, int, int, int, int, int, int] = field(default_factory=tuple)
    # ↓ used for saving, type of List[PIL.Image], but fuck PIL imports ffs
    mipmap_images: List[None] = field(default_factory=list)
    # ↓ used for displaying in DPG
    mipmap_arrays: List[np.array] = field(default_factory=list)


@dataclass(slots=True)
class TextureCache:
    texture: Texture = field(default_factory=Texture)
    file: Path = None
    path: Path = None
    image_type: str = None
    file_list: Tuple[Path] = field(default_factory=tuple)
    file_list_size: int = None
    viewer_position: int = None

    def save_image(self, sender, app_data, file_ext):
        if self.texture.mipmap_images:
            try:
                image_filename = self.file.stem.removesuffix("-C")
                with dpg.mutex():
                    filename = filepicker(
                        filepicker_type="SAVE_FILE",
                        title="Save as...",
                        initialfile=f"{image_filename}.{file_ext}",
                        filetypes=(("Image", f"*.{file_ext}"),),
                    )
                if filename:
                    # save last available mipmap - skip all corrupted mipmaps
                    last_available = 0
                    for i, mipmap in enumerate(self.texture.mipmap_images):
                        if mipmap is not None:
                            last_available = i
                    if self.texture.mipmap_images[last_available]:
                        self.texture.mipmap_images[last_available].save(filename)
            except Exception as error:
                dpg.configure_item("error_popup", show=True)
                wrapper = TextWrapper(width=80)
                dpg.configure_item(
                    "error_popup_message_input",
                    default_value=wrapper.fill(text=str(error)),
                )

    def save_tex_file(self, sender, app_data, user_data):
        if self.texture.mipmap_images:
            try:
                colorspace = dpg.get_value("ztex_colorspace_combo")
                format_id = (
                    COLORSPACE_LOOKUP[colorspace]
                    if colorspace in COLORSPACE_LOOKUP
                    else None
                )
                zTexture.save(
                    # Common image, mipmap at always 0
                    self.texture.mipmap_images[0],
                    Path(dpg.get_value("filepath_ztex_save_input")),
                    generate_mipmaps=dpg.get_value("generate_mipmaps_checkbox"),
                    format_param=format_id,
                    size_correction_mode=dpg.get_value(
                        "texture_correction_mode_combo"
                    ).upper(),
                )
                dpg.configure_item("ztex_save_popup", show=False)
            except Exception as error:
                dpg.configure_item("error_popup", show=True)
                wrapper = TextWrapper(width=80)
                dpg.configure_item(
                    "error_popup_message_input",
                    default_value=wrapper.fill(text=str(error)),
                )

    def switch_viewer(self, sender, app_data, direction, pos=None):
        if any(dpg.get_item_state(popup)["visible"] for popup in (
                "splash_popup",
                "error_popup",
                "ztex_save_popup"
            )):
            return

        if dpg.get_value("main_bar") != dpg.get_alias_id("viewer_tab"):
            return

        if self.viewer_position is None:
            return

        if pos is None:
            pos = self.viewer_position + direction

        if pos < 0 or self.file_list_size - 1 < pos:
            return

        # meh, workaround, too tired and lazy to fuck around with the list
        if self.file_list[pos] == "INVALID" and direction in (-1, 1):
            pos = self.viewer_position + (2 * direction)

        self.update(sender, app_data, self.file_list[pos])

    def update(self, sender, app_data, path):
        if path == "INVALID":
            dpg.configure_item("VIEWER_WINDOW", show=False)
            dpg.configure_item("invalid_file_button", show=True)
            dpg.configure_viewport(item=0, title=f"{PROGRAM_TITLE} Invalid File...")
            return

        if not path or not path.exists():
            dpg.configure_item("splash_popup", show=True)
            return

        dpg.configure_item("invalid_file_button", show=False)
        dpg.configure_item("splash_popup", show=False)
        dpg.configure_item("splash_invalid_files", show=False)
        dpg.configure_item("VIEWER_WINDOW", show=True)

        if path.is_file():
            file = path
            path = path.parent
        else:
            file = None
            path = path

        if self.path != path:
            self.file_list = tuple(
                p.resolve() for p in path.iterdir() if p.suffix.lower() in ALL_EXT
            )
            self.path = path

            dpg.delete_item("file_position_items", children_only=True)
            self.file_list_size = 0
            dpg.add_button(
                parent="file_position_items",
                label="Open File...",
                callback=filepicker_callback,
                user_data=(self, "ASK_FILE", None),
                width=-1,
            )
            dpg.add_button(
                parent="file_position_items",
                label="Open Directory...",
                callback=filepicker_callback,
                user_data=(self, "ASK_DIRECTORY", None),
                width=-1,
            )
            for i, file_item in enumerate(self.file_list):
                self.file_list_size += 1
                entry = file_item.name
                dpg.add_button(
                    parent="file_position_items",
                    label=f"{self.file_list_size} {entry}",
                    tag=f"{i}_{entry}",
                    width=-1,
                    user_data=i,  # ←←←←←←←←←←←←←←←←←←↓↓↓↓↓↓↓↓
                    callback=lambda sender, app_data, position: self.switch_viewer(
                        sender, app_data, direction=None, pos=position
                    ),
                )
            print(f"Found {self.file_list_size} files")

        if self.file is None or self.file != file:  # viewer
            try:
                if file is None:
                    self.file = self.file_list[0]
                    self.viewer_position = 0
                else:
                    self.file = file
                    self.viewer_position = self.file_list.index(file)

                dpg.configure_item(
                    "amount_of_files",
                    label=f"{self.viewer_position+1}/{self.file_list_size}",
                )

                dpg.set_value("batch_convert_input_path", self.path)
                if dpg.get_value("batch_convert_mode") == BATCH_MODE[0]:
                    dpg.set_value(
                        "batch_convert_output_path", self.path / "_decompiled"
                    )
                else:
                    dpg.set_value("batch_convert_output_path", self.path / "_compiled")
            except (IndexError, ValueError):
                dpg.configure_viewport(item=0, title=PROGRAM_TITLE)
                dpg.set_value("batch_convert_input_path", "")
                dpg.set_value("batch_convert_output_path", "")
                dpg.configure_item("splash_invalid_files", show=True)
                self.update("reset", "", None)
                return

            raw_texture = load_texture(self.file)
            if raw_texture is None:
                self.remove(self.file)
                return

            image_type, header, image_data = raw_texture
            self.image_type = image_type
            self.save_as_handler()
            mipmap_arrays = convert_texture(image_data)
            with dpg.mutex():
                if dpg.does_item_exist("mipmap_group_buttons"):
                    dpg.delete_item("mipmap_group_buttons", children_only=True)
                dpg.delete_item("axisx", children_only=True)
                dpg.delete_item("viewer_registry", children_only=True)
                self.texture = Texture(
                    mipmap_images=image_data, mipmap_arrays=mipmap_arrays, header=header
                )
                self.update_viewer()

        if dpg.get_value("main_bar") == dpg.get_alias_id("viewer_tab"):
            dpg.configure_viewport(item=0, title=f"{PROGRAM_TITLE} {self.file}")
        else:
            dpg.configure_viewport(item=0, title=f"{PROGRAM_TITLE} {self.path}")

        if not self.texture.mipmap_arrays:
            self.update("reset", "", None)

    def remove(self, file: Path):
        for i, item in enumerate(self.file_list):
            if file == item:
                self.file_list[i] = "INVALID"
                label = dpg.get_item_configuration(f"{i}_{file.name}")["label"]
                dpg.configure_item(f"{i}_{file.name}", label=f"Invalid file: {label}")
                print(f"[Remove] Marking {file} as invalid in cache...")
                break
        print("[Remove] Opening previous file.")
        self.update("remove", file, self.file_list[self.viewer_position])

    def update_viewer(self):
        def switch_to_mipmap(sender, app_data, user_data):
            level, mipmap_count = user_data
            if dpg.does_item_exist(f"image_series{level}"):
                for i in range(0, mipmap_count):
                    if dpg.does_item_exist(f"image_series{i}"):
                        dpg.configure_item(f"image_series{i}", show=False)
                dpg.configure_item(f"image_series{level}", show=True)
                dpg.fit_axis_data("axisx")
                dpg.fit_axis_data("axisy")

        # ram cleanup > look here: https://gitlab.com/-/snippets/3750194
        # It's done like this because lambda: in a loop is a no-no
        def nuke_ram(sender, app_data, texture_id):
            if dpg.does_item_exist(texture_id):
                dpg.set_value(texture_id, ())

        format, width, height, mipmap_count, _, _, average_color = self.texture.header
        reverso = mipmap_count  # Reverse order (desc.) for user output, keep it asc. in scripts for consistency
        corrupted_mipmap = 0
        valid_mipmap = 0
        frame = dpg.get_frame_count() + 2
        for i, mipmap in enumerate(self.texture.mipmap_images):
            mipmap_array = self.texture.mipmap_arrays[i]
            if reverso > 0:
                reverso -= 1
            if mipmap is not None or mipmap_array is not None:
                valid_mipmap += 1
                dpg.add_button(
                    label=f"{reverso}",
                    parent="mipmap_group_buttons",
                    callback=switch_to_mipmap,
                    user_data=(i, mipmap_count),
                )
                w = mipmap.width
                h = mipmap.height
                dpg.add_static_texture(
                    width=w,
                    height=h,
                    default_value=mipmap_array,
                    parent="viewer_registry",
                    tag=f"texture_tag{i}",
                )
                # print(f"texture_tag{i} defined {dpg.get_frame_count()}")
                dpg.add_image_series(
                    f"texture_tag{i}",
                    show=False,
                    bounds_min=(0, 0),
                    bounds_max=(w, h),
                    parent="axisx",
                    tag=f"image_series{i}",
                )
                frame += 2
                dpg.set_frame_callback(frame, callback=nuke_ram, user_data=f"texture_tag{i}")
            else:
                corrupted_mipmap += 1
                dpg.add_button(
                    label=f"{reverso}",
                    parent="mipmap_group_buttons",
                    tag=f"corrupted_mipmap{i}",
                )
                dpg.bind_item_theme(f"corrupted_mipmap{i}", "theme_warning")

        mipmap_info = mipmap_count
        dimensions = f"{width}x{height}"
        if corrupted_mipmap > 0:
            mipmap_info = f"{mipmap_count} ({corrupted_mipmap} corrupted)"
            dimensions = f"{width}x{height}?"

        if self.image_type == "COMMON_IMAGE":
            misc_info = (
                f"(Image mode: {format}) ({dimensions}) (Mipmaps: {valid_mipmap})"
            )
        else:
            misc_info = f"(Format: {FORMAT_PRINT_LOOKUP[format]}) ({dimensions}) (Average Color:{hex(average_color)}) (Mipmaps: {mipmap_info})"

        dpg.configure_item("texture_plot", label=misc_info)
        valid_mipmap = 0 if valid_mipmap == 0 else valid_mipmap - 1
        dpg.set_value("filepath_ztex_save_input", f"{self.path / self.file.stem}-C.TEX")
        switch_to_mipmap(None, None, (valid_mipmap, mipmap_count))

    def save_as_handler(self):
        if self.image_type == "COMMON_IMAGE":  # Saving as common image
            dpg.configure_item("save_as_common_image_menu", show=False)
            dpg.configure_item("save_as_tex_menu", show=True)
        else:  # Saving as zengin .TEX
            dpg.configure_item("save_as_common_image_menu", show=True)
            dpg.configure_item("save_as_tex_menu", show=False)


def filepicker_callback(sender, app_data, user_data):
    cache, path_type, external_input = user_data
    output_path = None
    with dpg.mutex():
        if path_type == "ASK_DIRECTORY":
            output_path = filepicker(
                filepicker_type=path_type, title="Open Directory..."
            )
        elif path_type == "ASK_FILE":
            output_path = filepicker(
                filepicker_type=path_type,
                title="Open file...",
                filetypes=[("Supported formats", "*.TEX *.TGA *.PNG *.WEBP *.DDS")],
            )
        elif path_type == "SAVE_FILE":
            output_path = filepicker(
                filepicker_type=path_type,
                title="Save as TEX...",
                initialfile=f"{cache.file.stem}-C.TEX",
                filetypes=[(r"zengin TEX", "*.TEX")],
            )

    if output_path:
        if external_input is not None:
            dpg.set_value(external_input, output_path)
            return

        cache.update(sender, app_data, Path(output_path))


def tabbar_callback(sender, app_data, user_data):
    cache = user_data
    if cache.file:
        if app_data == dpg.get_alias_id("viewer_tab"):
            dpg.configure_viewport(item=0, title=f"{PROGRAM_TITLE} {cache.file}")
    else:
        dpg.configure_viewport(item=0, title=f"{PROGRAM_TITLE} Invalid path/file")


def batch_convert_mode_callback(sender, app_data, user_data):
    cache = user_data
    if app_data == BATCH_MODE[0]:
        dpg.set_value("batch_convert_format_text", FORMAT_MODE[0])
        dpg.set_value("batch_convert_format", SUPPORTED_WRITE_IMAGE_FORMATS[0])
        dpg.configure_item("batch_convert_format", items=SUPPORTED_WRITE_IMAGE_FORMATS)
        dpg.configure_item("batch_convert_totex_only_options", show=False)
        dpg.set_value("batch_convert_output_path", cache.path / "_decompiled")
    else:
        dpg.set_value("batch_convert_format_text", FORMAT_MODE[1])
        dpg.set_value("batch_convert_format", ZTEX_COLORSPACE[0])
        dpg.configure_item("batch_convert_format", items=ZTEX_COLORSPACE)
        dpg.configure_item("batch_convert_totex_only_options", show=True)
        dpg.set_value("batch_convert_output_path", cache.path / "_compiled")


def batch_convert(sender, app_data, user_data):
    def run_process(parameters):
        with Popen(
            parameters,
            cwd=CURRENT_PATH,
            stdout=PIPE,
            stderr=STDOUT,
            bufsize=1,
            text=True,
            shell=False,
        ) as process:
            while process.poll() is None:
                for line in process.stdout:
                    new_text = dpg.get_value("batch_convert_log_status") + line
                    dpg.set_value("batch_convert_log_status", new_text)

    dpg.configure_item("batch_convert_apply_button", show=False)
    dpg.configure_item("viewer_tab", show=False)

    mode = dpg.get_value("batch_convert_mode")
    input_path = Path(dpg.get_value("batch_convert_input_path"))
    output_path = Path(dpg.get_value("batch_convert_output_path"))
    format = dpg.get_value("batch_convert_format")  #

    overwrite = dpg.get_value("batch_convert_overwrite_files")  # bool
    # RESIZE, EXTEND
    size_correction_mode = dpg.get_value("batch_convert_size_correction_modes")
    generate_mipmaps = dpg.get_value("batch_convert_generate_mipmaps")  # bool

    dpg.set_value("batch_convert_log_status", "")

    if not input_path.exists() or dpg.get_value("batch_convert_input_path") == "":
        dpg.set_value("batch_convert_log_status", "Invalid input path.")
        return
    
    parameters = list()

    if CURRENT_EXECUTABLE.name.upper().endswith(".PY"):
        parameters.extend(("python", CURRENT_EXECUTABLE))
    else:
        parameters.append(CURRENT_EXECUTABLE)

    if mode == BATCH_MODE[0]:
        parameters.extend((
                "-ft",
                str(input_path),
                str(output_path),
                "-f",
                str(format),
                "-o",
                str(overwrite),
            ))
    else:
        try:
            colorspace = COLORSPACE_LOOKUP[format]
        except:
            colorspace = None
        parameters.extend((
                "-tt",
                str(input_path),
                str(output_path),
                "-f",
                str(colorspace),
                "-o",
                str(overwrite),
                "-gm",
                str(generate_mipmaps),
                "-scm",
                str(size_correction_mode),
            ))
    run_process(parameters)

    dpg.configure_item("viewer_tab", show=True)
    dpg.configure_item("batch_convert_apply_button", show=True)


def init_error_popup():
    PopupFactory("error_popup", 700, 350, title="Error!", title_bar=True)
    with dpg.group(parent="error_popup"):
        dpg.add_input_text(
            enabled=False,
            height=270,
            default_value="",
            width=-1,
            multiline=True,
            tag="error_popup_message_input",
            parent="error_popup",
        )
        dpg.add_button(
            label="OK",
            parent="error_popup",
            callback=lambda: dpg.configure_item("error_popup", show=False),
            width=-1,
        )


def init_splash_popup(cache: TextureCache):
    PopupFactory("splash_popup", 700, 185, title=f"zTEXiPy {VERSION}", title_bar=True)
    with dpg.group(parent="splash_popup"):
        # kekw
        dpg.add_button(
            label="Put directory or file here...                                            ",
            width=-1,
            enabled=False,
        )
        # kekw
        with LayoutHelper() as l:
            l.add_widget(dpg.add_input_text(tag="filepath_splash_input"), 70)
            l.add_widget(
                dpg.add_button(
                    label="...",
                    callback=filepicker_callback,
                    user_data=(cache, "ASK_FILE", "filepath_splash_input"),
                ),
                20,
            )

        with LayoutHelper() as l:
            l.add_widget(
                dpg.add_button(
                    label="Open",
                    callback=lambda: cache.update(
                        "splash_setup",
                        None,
                        Path(dpg.get_value("filepath_splash_input")),
                    ),
                ),
                50,
            )
            l.add_widget(
                dpg.add_button(label="Exit", callback=lambda: dpg.stop_dearpygui()), 50
            )

        with dpg.group(tag="splash_invalid_files", show=False):
            dpg.add_separator()
            dpg.add_button(
                label="No valid file/directory was chosen, try again.",
                width=-1,
                enabled=False,
            )


def init_advsave_popup(cache: TextureCache):
    PopupFactory("ztex_save_popup", 700, 225, title="Save options...", title_bar=True)
    with dpg.group(parent="ztex_save_popup"):
        dpg.add_checkbox(
            label="Generate mipmaps",
            default_value=True,
            tag="generate_mipmaps_checkbox",
        )
        with LayoutHelper() as l:
            l.add_widget(dpg.add_button(label="Colorspace:"), 30)
            l.add_widget(
                dpg.add_combo(
                    ZTEX_COLORSPACE,
                    default_value=ZTEX_COLORSPACE[0],
                    tag="ztex_colorspace_combo",
                ),
                70,
            )

        with LayoutHelper() as l:
            l.add_widget(dpg.add_button(label="Size correction mode:"), 30)
            l.add_widget(
                dpg.add_combo(
                    ("Extend", "Resize"),
                    default_value="Extend",
                    tag="texture_correction_mode_combo",
                ),
                70,
            )

        with LayoutHelper() as l:
            l.add_widget(dpg.add_input_text(tag="filepath_ztex_save_input"), 70)
            l.add_widget(
                dpg.add_button(
                    label="...",
                    callback=filepicker_callback,
                    user_data=(cache, "SAVE_FILE", "filepath_ztex_save_input"),
                ),
                20,
            )

        with LayoutHelper() as l:
            l.add_widget(dpg.add_button(label="Save", callback=cache.save_tex_file), 50)
            l.add_widget(
                dpg.add_button(
                    label="Cancel",
                    callback=lambda: dpg.configure_item("ztex_save_popup", show=False),
                ),
                50,
            )


def init_file_position_popup(cache: TextureCache):
    PopupFactory(
        "file_position_popup",
        700,
        575,
        title="Files in folder...",
        title_bar=True,
        moving=True,
        modal=False,
        resizable=True,
    )
    with dpg.group(parent="file_position_popup"):
        dpg.add_child_window(tag="file_position_items", width=-1, height=-35)
        dpg.add_button(
            label="OK",
            callback=lambda: dpg.configure_item("file_position_popup", show=False),
            width=-1,
        )


def init_popups(cache: TextureCache):
    init_error_popup()
    init_splash_popup(cache)
    init_advsave_popup(cache)
    init_file_position_popup(cache)


@dataclass(slots=True)
class DearPyGui:
    title: str = PROGRAM_TITLE
    autosize: bool = True
    no_collapse: bool = True
    fullscreen: bool = False
    no_close: bool = True
    min_width: int = 800
    min_height: int = 450
    show: bool = True
    maximized: bool = True
    callbacks: List[None] = field(default_factory=list)
    font: Path = FONT_PATH
    theme: Path = THEME_PATH

    def init_viewport(self):
        dpg.create_viewport()
        dpg.configure_viewport(
            item=0,
            autosize=self.autosize,
            no_collapse=self.no_collapse,
            fullscreen=self.fullscreen,
            no_close=self.no_close,
            vsync=True,
            min_width=self.min_width,
            min_height=self.min_height,
            x_pos=0,
            y_pos=0,
            no_scrollbar=True,
            title=self.title,
        )

        if self.show:
            dpg.show_viewport()
        if self.maximized:
            dpg.maximize_viewport()

    def theme_loader(self):
        try:
            with open(self.theme, "r") as fh:
                colorscheme = json_load(fh)
        except:
            return

        if dpg.does_item_exist("global"):
            dpg.delete_item("global")
        with dpg.theme(tag="global"):
            with dpg.theme_component(0):
                dpg.add_theme_style(dpg.mvStyleVar_ScrollbarRounding, 0)
                dpg.add_theme_style(dpg.mvStyleVar_ScrollbarSize, 28)
                dpg.add_theme_style(dpg.mvStyleVar_GrabMinSize, 15)
                for key, rgba in colorscheme.items():
                    if key.startswith("mvThemeCol"):
                        dpg.add_theme_color(getattr(dpg, key), rgba, tag=f"color_{key}")
                    elif key.startswith("mvPlotCol"):
                        dpg.add_theme_color(
                            getattr(dpg, key),
                            rgba,
                            tag=f"color_{key}",
                            category=dpg.mvThemeCat_Plots,
                        )
                    elif key.startswith("mvNodeCol"):
                        dpg.add_theme_color(
                            getattr(dpg, key),
                            rgba,
                            tag=f"color_{key}",
                            category=dpg.mvThemeCat_Nodes,
                        )
        dpg.bind_theme("global")

    def register_fonts(self):
        if self.font and self.font.exists():
            with dpg.font_registry():
                with dpg.font(self.font, FONT_SIZE, tag="default_font"):
                    dpg.add_font_range_hint(dpg.mvFontRangeHint_Default)
                    dpg.add_font_range_hint(dpg.mvFontRangeHint_Cyrillic)
                    dpg.add_font_range(0x0104, 0x017C)
                    dpg.add_font_chars([0x25BC, 0x25BA])
                dpg.bind_font("default_font")

    def __enter__(self):
        dpg.create_context()
        dpg.setup_dearpygui()
        self.theme_loader()
        self.register_fonts()
        return self

    def __exit__(self, exc_type, exc_value, exc_traceback):
        self.init_viewport()
        for func in self.callbacks:
            func()

        if DEBUG:
            # import dearpygui.demo as demo
            # demo.show_demo()

            val = None
            while dpg.is_dearpygui_running():
                # try:
                #     stamp = THEME_PATH.lstat().st_mtime
                #     if stamp != val:
                #         val = stamp
                #         print("Theme refreshed")
                #         self.theme_loader(file=THEME_PATH)
                # except:
                #     pass
                dpg.render_dearpygui_frame()
        else:
            dpg.start_dearpygui()
        dpg.destroy_context()


def start_viewer(path=None):

    # def cweluch():
    #     import os,psutil,time
    #     from threading import Thread

    #     def get_process_memory():
    #         process = psutil.Process(os.getpid())
    #         return process.memory_info().rss

    #     def meme():
    #         while True:
    #             time.sleep(0.01)
    #             print(f"Ram: {get_process_memory()}")

    #     T = Thread(target = meme)
    #     T.setDaemon(True)
    #     T.start()

    # cweluch()

    try:
        path = Path(path)
    except:
        path = None

    cache = TextureCache()

    def callback():
        init_popups(cache)
        dpg.set_frame_callback(1, cache.update, user_data=path)

    with DearPyGui(callbacks=(callback,)):
        if sys.platform == "win32":
            drag_and_drop(cache)
            from ctypes import windll

            windll.shell32.SetCurrentProcessExplicitAppUserModelID(
                "CompanyName.ProductName.SubProduct.VersionInformation"
            )
            windll.shcore.SetProcessDpiAwareness(2)
            if not DEBUG:
                hWnd = windll.kernel32.GetConsoleWindow()
                windll.user32.ShowWindow(hWnd, 0)

        with dpg.theme(tag="theme_warning"):
            with dpg.theme_component(dpg.mvAll):
                dpg.add_theme_color(
                    dpg.mvThemeCol_Text, (255, 0, 0), category=dpg.mvThemeCat_Core
                )
        dpg.add_texture_registry(tag="viewer_registry")

        with dpg.window(tag="WINDOW_PANEL"):
            dpg.set_primary_window("WINDOW_PANEL", True)
            with dpg.tab_bar(
                tag="main_bar", callback=tabbar_callback, user_data=(cache)
            ):
                with dpg.tab(label="Viewer", tag="viewer_tab"):
                    with dpg.handler_registry():
                        dpg.add_key_press_handler(
                            dpg.mvKey_A, callback=cache.switch_viewer, user_data=-1
                        )
                        dpg.add_key_press_handler(
                            dpg.mvKey_W, callback=cache.switch_viewer, user_data=5
                        )
                        dpg.add_key_press_handler(
                            dpg.mvKey_D, callback=cache.switch_viewer, user_data=1
                        )
                        dpg.add_key_press_handler(
                            dpg.mvKey_S, callback=cache.switch_viewer, user_data=-5
                        )

                        dpg.add_key_press_handler(
                            dpg.mvKey_Left, callback=cache.switch_viewer, user_data=-1
                        )
                        dpg.add_key_press_handler(
                            dpg.mvKey_Up, callback=cache.switch_viewer, user_data=5
                        )
                        dpg.add_key_press_handler(
                            dpg.mvKey_Right, callback=cache.switch_viewer, user_data=1
                        )
                        dpg.add_key_press_handler(
                            dpg.mvKey_Down, callback=cache.switch_viewer, user_data=-5
                        )
                    dpg.add_button(
                        tag="invalid_file_button",
                        show=False,
                        label="Invalid file. Press to open file selector.",
                        height=-1,
                        width=-1,
                        callback=lambda: dpg.configure_item(
                            "file_position_popup", show=True
                        ),
                    )
                    with dpg.child_window(
                        tag="VIEWER_WINDOW",
                        width=-1,
                        height=-1,
                        no_scrollbar=True,
                        menubar=True,
                        border=False,
                    ):
                        with dpg.menu_bar(tag="viewer_menu"):
                            with dpg.group(tag="save_as_handler"):
                                with dpg.menu(label="Open...", tag="open_menu"):
                                    dpg.add_menu_item(
                                        label="File...",
                                        callback=filepicker_callback,
                                        user_data=(cache, "ASK_FILE", None),
                                    )
                                    dpg.add_menu_item(
                                        label="Directory...",
                                        callback=filepicker_callback,
                                        user_data=(cache, "ASK_DIRECTORY", None),
                                    )
                                dpg.add_button(
                                    label="Save TEX as...",
                                    tag="save_as_tex_menu",
                                    show=False,
                                    callback=lambda: dpg.configure_item(
                                        "ztex_save_popup", show=True
                                    ),
                                )
                                with dpg.menu(
                                    label="Save as...",
                                    tag="save_as_common_image_menu",
                                    show=False,
                                ):
                                    for ext in SUPPORTED_WRITE_IMAGE_FORMATS:
                                        dpg.add_menu_item(
                                            label=ext,
                                            callback=cache.save_image,
                                            user_data=ext,
                                        )

                            dpg.add_button(
                                callback=cache.switch_viewer,
                                user_data=-1,
                                arrow=True,
                                direction=dpg.mvDir_Left,
                            )
                            dpg.add_button(
                                label="",
                                tag="amount_of_files",
                                callback=lambda: dpg.configure_item(
                                    "file_position_popup", show=True
                                ),
                            )
                            dpg.add_button(
                                callback=cache.switch_viewer,
                                user_data=1,
                                arrow=True,
                                direction=dpg.mvDir_Right,
                            )
                            dpg.add_button(label="   Mipmaps:")
                            dpg.add_group(horizontal=True, tag="mipmap_group_buttons")
                            dpg.add_slider_int(
                                tag="alpha_helper_drag",
                                width=-1,
                                format="Alpha helper: %i%%",
                                callback=lambda sender, app_data, user_data: dpg.set_value(
                                    "color_mvPlotCol_PlotBg",
                                    (150, 150, 150, int(app_data * 2.55)),
                                ),
                            )

                        with dpg.plot(
                            tag="texture_plot",
                            equal_aspects=True,
                            no_child=True,
                            width=-1,
                            height=-1,
                            crosshairs=True,
                            no_menus=True,
                            show=True,
                        ):
                            dpg.add_plot_axis(
                                dpg.mvXAxis,
                                tag="axisx",
                                no_tick_labels=True,
                                no_tick_marks=True,
                                no_gridlines=True,
                            )
                            dpg.add_plot_axis(
                                dpg.mvYAxis,
                                tag="axisy",
                                no_tick_labels=True,
                                no_tick_marks=True,
                                no_gridlines=True,
                            )
                with dpg.tab(label="Batch conversion", tag="converter_tab"):
                    with dpg.child_window(
                        tag="CONVERTER_WINDOW",
                        width=-1,
                        height=-1,
                        no_scrollbar=True,
                        border=False,
                    ):
                        with LayoutHelper() as l:
                            l.add_widget(dpg.add_button(label="Input path:"), 10)
                            l.add_widget(
                                dpg.add_input_text(tag="batch_convert_input_path"), 70
                            )
                        with LayoutHelper() as l:
                            l.add_widget(dpg.add_button(label="Output path:"), 10)
                            l.add_widget(
                                dpg.add_input_text(tag="batch_convert_output_path"), 70
                            )

                        with LayoutHelper() as l:
                            l.add_widget(
                                dpg.add_group(tag="batch_convert_mode_handler"), 50
                            )
                            l.add_widget(
                                dpg.add_group(tag="batch_convert_format_handler"), 50
                            )

                        with dpg.group(parent="batch_convert_mode_handler"):
                            dpg.add_text("Convert:")
                            dpg.add_combo(
                                items=BATCH_MODE,
                                default_value=BATCH_MODE[0],
                                tag="batch_convert_mode",
                                callback=batch_convert_mode_callback,
                                user_data=cache,
                            )
                        with dpg.group(parent="batch_convert_format_handler"):
                            dpg.add_text(
                                FORMAT_MODE[0], tag="batch_convert_format_text"
                            )
                            dpg.add_combo(
                                items=SUPPORTED_WRITE_IMAGE_FORMATS,
                                default_value=SUPPORTED_WRITE_IMAGE_FORMATS[0],
                                tag="batch_convert_format",
                            )
                        dpg.add_checkbox(
                            label="Overwrite Existing Files",
                            default_value=False,
                            tag="batch_convert_overwrite_files",
                        )
                        with dpg.group(
                            tag="batch_convert_totex_only_options", show=False
                        ):
                            with dpg.group():
                                dpg.add_checkbox(
                                    label="Generate Mipmaps",
                                    default_value=True,
                                    tag="batch_convert_generate_mipmaps",
                                )
                            dpg.add_separator()
                            with dpg.group():
                                dpg.add_text("Size Correction Mode:")
                                dpg.add_combo(
                                    items=SIZE_CORRECTION_MODE,
                                    default_value=SIZE_CORRECTION_MODE[0],
                                    tag="batch_convert_size_correction_modes",
                                    width=-1,
                                )

                        dpg.add_separator()
                        with LayoutHelper() as l:
                            l.add_widget(
                                dpg.add_button(
                                    label="Convert...",
                                    tag="batch_convert_apply_button",
                                    callback=batch_convert,
                                ),
                                50,
                            )
                        dpg.add_separator()
                        dpg.add_input_text(
                            tag="batch_convert_log_status",
                            multiline=True,
                            width=-1,
                            height=-1,
                            enabled=False,
                        )


# unused for now.
def start_batch_ui():
    with DearPyGui(title=f"{PROGRAM_TITLE} Batch Convert"):
        with dpg.window(tag="WINDOW_PANEL"):
            dpg.set_primary_window("WINDOW_PANEL", True)
            with dpg.group(tag="start_task"):
                dpg.add_text("Do you wish to start task?")
                dpg.add_button(label="Yes")
                dpg.add_button(label="No", callback=lambda: dpg.stop_dearpygui())
            with dpg.group(tag="cancel_task", show=False):
                dpg.add_button(label="Cancel", callback=lambda: dpg.stop_dearpygui())
