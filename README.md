# zTEXiPy

![Snaky boi painting a picture wearing fancy noble cloths](logo.webp)

### zTEXiPy is an open source project written in Python that utilizes the Pillow, NumPy, DearPyGui and libsquish libraries to convert Gothic and Gothic 2 (ZenGin) textures to common formats and vice versa.

## Download and installation
[Download package from release page](https://gitlab.com/Shoun2137/ztexipy/-/releases)
- Miss me with that command line stuff!:
	1. Unpack the package at your chosen installation path.
	2. Run `install.cmd` as normal user.
	- This batch command should invoke a Powershell script that runs regedit.exe and adds new registry files. It'll ask you about administrative privileges, press Yes button.
	3. You can open textures now with a simple viewer, and also you will get new context menus for various common formats!
	- Uninstallation:
		1. Run `uninstall.cmd` as Administrator.
- For Linux users:

	1. Run the program using the commandline `zTEXiPy` for basic usage information or use -GUI
	2. Sadly as there are several filemanagers, I can't provide context menus, it'd be a suicide. Such is Linux's user life tho. ;/
- For developers:
	1. Clone the zTEXiPy repository to your local machine.
	2. Compile the libsquish-bind project.
	3. Place compiled libsquish-bind libraries beside `squish.py` file.
		- Refer to the libsquish-bind README for more information.
	4. Install [uv](https://github.com/astral-sh/uv) - uv will take care of venv and python environment, you don't have to install anything else.
	5. Run the program using the command `uv run init.py` for basic usage information.
	- Building package:
		1. Get [upx](https://github.com/upx/upx/releases/tag/v4.0.2) and unpack it. Pyinstaller doesn't work with upx on Linux, so you can skip this.
		2. Run pyinstaller depending on your system: 
		- Linux: `uv run pyinstaller build_linux.spec -y`
		- Windows: `uv run PyInstaller build_windows.spec -y`
		- Windows with upx: `uv run PyInstaller build_windows.spec -y --upx-dir=D:\path\to\upx\`
		- This should build standalone program. Remember to place libsquish-bind library in the same directory as zTEXiPy executable.

## Requirements

- Python 3.12.6 or later
- Pillow library
- NumPy library
- DearPyGui library
	- [DearPyGui-DragAndDrop](https://github.com/IvanNazaruk/DearPyGui-DragAndDrop) (For Windows only)
- libsquish-bind (Found in this repository)

### Only 64-bit systems are supported.

## Features

- Converts Gothic and Gothic 2 textures to various formats including TGA, PNG, WEBP, JPG.
Check full support at [Pillow documentation](https://pillow.readthedocs.io/en/stable/handbook/image-file-formats.html)
- Allows users to batch convert multiple textures at once.
- Supports compression to DXT1 (BC1), DXT3 (BC2) and DXT5 (BC3) (DXT5 is not supported by ZenGin... yet?).

## Contributing

Contributions to zTEXiPy are welcome! If you want to contribute, please fork the repository and create a new branch for your changes. Once your changes are complete, submit a pull request for review.

## License

zTEXiPy is released under the GPLv3 License.

## Community

zTEXiPy couldn't exists without extensive support from the [Gothic Modding Community](https://gothic-modding-community.github.io/gmc/). Please check out our [Discord server](https://discord.gg/mCpS5b5SUY).

## Special thanks to:
- Luis "lmichaelis" Michaelis - For [phoenix project](https://github.com/GothicKit/phoenix) as it was used as a reference for how ZenGin textures work.
- Patrix - For his g2o module template as it was used as a basis for libsquish-bind cmake project.
- Kamil "HRY" Krzyśków - For his insight about various Python stuff.
- ChatGPT - For writing tedious stuff, such us this markdown document.
- The Chronicles of Myrtana Team - Their assets were used for creating logo and icon.
- mCoding - For his [video about multiprocessing](https://www.youtube.com/watch?v=X7vBbelRXn0).
- IvanNazaruk - For his [excellent drag and drop script](https://github.com/IvanNazaruk/DearPyGui-DragAndDrop) for dearpygui
- mathgeniuszach - Parts of filepicker of [xdialog](https://github.com/mathgeniuszach/xdialog/tree/main)
- Astral - For [uv](https://github.com/astral-sh/uv) python package and project manager.